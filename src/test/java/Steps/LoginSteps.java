package Steps;

import Driver.WebDriverFactory;
import Pages.AbstractPage;
import Pages.LoginPage;
import cucumber.api.java8.En;

public class LoginSteps extends AbstractPage implements En {

    private static String merchantEmail;
    private static String merchantPassword;

    public LoginSteps() {

        Given("^I have browser with merchant login page$",
                () -> {
                    loginPage = new LoginPage(System.getProperty("merchantURL"), WebDriverFactory.create());
                    merchantEmail = System.getProperty("merchantEmail");
                    merchantPassword = System.getProperty("merchantPassword");
                });

        When("^Enter merchant email$", () -> loginPage.email(merchantEmail));

        When("^Enter merchant password$", () -> loginPage.password(merchantPassword));

        When("^Click login button$", () -> homePage = loginPage.login());

        When("^Close browser$", () -> WebDriverFactory.quit());
    }
}
