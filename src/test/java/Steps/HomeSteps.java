package Steps;

import Pages.AbstractPage;
import cucumber.api.java8.En;

public class HomeSteps extends AbstractPage implements En {

    private static long timestamp;

    public HomeSteps()
    {
        When("^User menu -> Logout$", () -> {
            homePage.userMenu();
            loginPage = homePage.logOut();
        });

        When("^Click Customer \\(on left side panel\\)$", () -> homePage.customers());

        When("^Click Not Registered tab$", () -> homePage.notRegisterdTab());

        When("^Click New button -> Create$", () -> homePage.createCustomer());

        When("^Enter unique customer CRM '(.*)'$", (String crm) -> {
            timestamp = System.currentTimeMillis();
            homePage.newCustomer.CRM(crm + timestamp);
        });

        When("^Enter customer name '(.*)'$", (String name) -> homePage.newCustomer.name(name));

        When("^Enter unique customer Email or phone '(.*)'$",(String text) -> homePage.newCustomer.emailOrPhone(text + System.currentTimeMillis()));

        When("^Click Save button$", () -> homePage.newCustomer.save());

        Then("Customer with unique CRM '(.*)' displayed in Not Registered table", (String crm) -> homePage.assertCustomerDisplayed(crm+timestamp));
    }
}
