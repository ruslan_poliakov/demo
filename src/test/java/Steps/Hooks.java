package Steps;

import Driver.WebDriverFactory;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

public class Hooks {

    @After
    public void addScreen(Scenario scenario) {
        if (scenario.isFailed()) {
            scenario.embed(((TakesScreenshot) WebDriverFactory.getDriver()).getScreenshotAs(OutputType.BYTES), "image/png");
        }
    }
}
