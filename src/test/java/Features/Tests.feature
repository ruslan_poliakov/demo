@DemoTest

Feature: Merchant Login/Logout, add new Customer

  Scenario: 1. Login as merchant
    Given I have browser with merchant login page
    When Enter merchant email
    And Enter merchant password
    And Click login button

  Scenario: 2. Logout
    When User menu -> Logout

  Scenario: 3. Add new Customer
    When Enter merchant email
    And Enter merchant password
    And Click login button
    And Click Customer (on left side panel)
    And Click Not Registered tab
    And Click New button -> Create
    And Enter unique customer CRM 'Test_CRM'
    And Enter customer name 'Test_Name'
    And Enter unique customer Email or phone 'Te@st.test'
    And Click Save button
    Then Customer with unique CRM 'Test_CRM' displayed in Not Registered table

  #Scenario: 4. Example of test fail
  #  When Click New button -> Create
  #  And Enter unique customer CRM 'Test_CRM_FAIL'
  #  And Enter customer name 'Test_Name_FAIL'
  #  And Enter unique customer Email or phone 'Te@st.test'
  #  Then Customer with unique CRM 'Test_CRM' displayed in Not Registered table

  Scenario: Postconditions
    Given Close browser
