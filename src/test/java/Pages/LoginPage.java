package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class LoginPage extends AbstractPage {

    @FindBy(xpath = "//input[@type='email']")
    private WebElement email;

    @FindBy(xpath = "//input[@type='password']")
    private WebElement password;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement login;

    //default constructor
    public LoginPage() {
        this(null, null);
    }
    //initial constructor
    public LoginPage(String URL, WebDriver driver) {
        if (driver!=null) setDriver(driver);
        if (URL!=null) this.driver.get(URL);
        PageFactory.initElements(this.driver, this);
        wait.until(ExpectedConditions.visibilityOf(login));
    }

    public void email(String text) {
        email.sendKeys(text);
    }

    public void password(String text) {
        password.sendKeys(text);
    }

    public HomePage login() {
        login.click();
        return new HomePage();
    }
}
