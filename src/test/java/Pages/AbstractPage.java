package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class AbstractPage {
    protected static WebDriver driver;
    protected static WebDriverWait wait;
    protected static LoginPage loginPage;
    protected static HomePage homePage;

    public void setDriver(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 30);
    }
}
