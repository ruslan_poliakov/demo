package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;


public class HomePage extends AbstractPage {

    @FindBy(id = "header-dropdown")
    private WebElement userMenu;

    @FindBy(xpath = "//div[@class='mc-header']//ul//a[text()='Log Out']")
    private WebElement logOut;

    @FindBy(xpath = "//aside//ul//span[text()='Customers']")
    private WebElement customers;

    @FindBy(id = "customersTabs-tab-notRegistered")
    private WebElement notRegistered;

    @FindBy(id = "newCustomerDropdown")
    private WebElement newButton;

    @FindBy(xpath = "//div[@class='mc-box-top']//ul//a[text()='Create']")
    private WebElement create;

    @FindBy(xpath = "//div[@id='customersTabs-pane-notRegistered']//div[@class='_item clearfix']//h3")
    private List<WebElement> customerNames;

    public NewCustomer newCustomer;

    public HomePage() {
        PageFactory.initElements(driver, this);
        wait.until(ExpectedConditions.visibilityOf(userMenu));
    }

    public void userMenu() {
        userMenu.click();
    }

    public LoginPage logOut() {
        logOut.click();
        return new LoginPage();
    }

    public void customers() {
        customers.click();
    }

    public void notRegisterdTab() {
        notRegistered.click();
    }

    public void createCustomer()
    {
        newButton.click();
        create.click();
        newCustomer = new NewCustomer();
    }

    public void assertCustomerDisplayed(String customerName){
        wait.until(nameDisplayed -> customerNames.stream().anyMatch(name -> name.getText().equals(customerName) && name.isDisplayed()));
    }

    public class NewCustomer extends AbstractPage
    {
        public NewCustomer() {
            PageFactory.initElements(driver, this);
            wait.until(ExpectedConditions.visibilityOf(CRM));
        }

        @FindBy(xpath = "//input[@placeholder='CRM']")
        private WebElement CRM;

        @FindBy(xpath = "//input[@placeholder='Name']")
        private WebElement name;

        @FindBy(xpath = "//input[@placeholder='Email or Phone']")
        private WebElement emailOrPhone;

        @FindBy(xpath = "//div[@class='modal-dialog']//button[@type='submit']")
        private WebElement save;

        public void CRM(String text) {
            CRM.sendKeys(text);
        }

        public void name(String text) {
            name.sendKeys(text);
        }

        public void emailOrPhone(String text) {
            emailOrPhone.sendKeys(text);
        }

        public void save() {
            save.click();
        }
    }
}
