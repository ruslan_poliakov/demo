package CucumberRunner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;


@CucumberOptions(
        strict = true,
        plugin = {"pretty", "html:target/cucumber"},
        features = "src/test/java/Features",
        glue = "Steps",
        tags = "~@ignore")

@RunWith(Cucumber.class)
public class Runner {
}

